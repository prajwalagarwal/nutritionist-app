package com.stackroute.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.stackroute.model.UserModel;

@TestInstance(Lifecycle.PER_CLASS)
public class UserRepositoryTest {

	@Mock
	private UserRepository userRepository;

	private UserModel user;
	
	transient Optional<UserModel> options;

	@BeforeAll
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		user = new UserModel("ram", "1234", "ram@gmail.com", "Ram", "Agarwal");
		options=Optional.of(user);
	}

	@AfterAll
	public void delete() {
		userRepository.delete(user);
	}

	@Test
	public void testRegisterSuccess() {
		userRepository.save(user);
		when(userRepository.findById(user.getUsername())).thenReturn(options);
		Optional<UserModel> option = userRepository.findById(user.getUsername());
		assertThat(option.equals(user));
	}

	@Test
	public void findByUsername() {
		userRepository.save(user);
		Optional<UserModel> option = userRepository.findById(user.getUsername());
		assertThat(option.equals(user));
		assertThat(userRepository.findById("ram").equals(user.getUsername()));
	}
}
