package com.stackroute.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.stackroute.exception.UserAlreadyExistsException;
import com.stackroute.model.UserModel;
import com.stackroute.repository.UserRepository;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class UserServiceTest {
	@Mock
	private UserRepository userRepository;

	private UserModel user;

	@Mock
	private UserLoginService userService;

	@InjectMocks
	private UserLoginServiceImpl userServiceImpl;
	Optional<UserModel> options;
	Optional<UserModel> op = Optional.empty();

	@BeforeAll
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		user = new UserModel();
		user.setFirstName("Ram");
		user.setLastName("Agarwal");
		user.setUsername("ram");
		user.setPassword("1234");
		user.setEmail("test@gmail.com");
		options = Optional.of(user);
	}

	@Test
	@Order(1)
	public void testSaveUserSuccess() throws Exception {
		when(userRepository.findById(user.getUsername())).thenReturn(op);
		when(userRepository.save(user)).thenReturn(user);
		boolean flag = userServiceImpl.saveUser(user);
		assertEquals("can regsiter user", true, flag);
		verify(userRepository, times(1)).save(user);
	}

	@Test()
	@Order(2)
	public void testSaveUserFailure() throws UserAlreadyExistsException {
		when(userRepository.findById(user.getUsername())).thenReturn(options);
		boolean flag = userServiceImpl.saveUser(user);
		assertEquals("user exists", false, flag);
	}

	@Test
	@Order(3)
	public void testLoadUserDetail() throws Exception {
		when(userRepository.findById(user.getUsername())).thenReturn(options);
		UserModel userResult = (UserModel) userServiceImpl.loadUserByUsername(user.getUsername());
		assertNotNull(userResult);
		assertEquals("ram", userResult.getUsername());
	}

	@Test
	@Order(4)
	public void testLoadUserDetailNotFound() throws UsernameNotFoundException {
		when(userRepository.findById(user.getUsername())).thenThrow(new UsernameNotFoundException("User not found"));
		try {
			UserModel userResult = (UserModel) userServiceImpl.loadUserByUsername(user.getUsername());
		} catch (UsernameNotFoundException e) {
			assertTrue(true);
		}
	}

	@Test
	@Order(5)
	public void testNotExistingEmail() throws Exception {
		when(userRepository.findByEmail(user.getEmail())).thenReturn(null);
		boolean flag = userServiceImpl.existsByEmail(user.getEmail());
		assertEquals("Email does not exist", false, flag);
	}

	@Test
	@Order(6)
	public void testExistingEmail() throws UsernameNotFoundException {
		when(userRepository.findByEmail(user.getEmail())).thenReturn(user);
		boolean flag = userServiceImpl.existsByEmail(user.getEmail());
		assertEquals("Email exist", true, flag);
	}

}
