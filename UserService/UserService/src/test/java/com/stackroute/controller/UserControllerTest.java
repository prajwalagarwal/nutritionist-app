package com.stackroute.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.config.JwtTokenUtil;
import com.stackroute.exception.UserNotFoundException;
import com.stackroute.model.UserModel;
import com.stackroute.service.UserLoginService;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class UserControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@MockBean
	PasswordEncoder encoder;

	ObjectMapper om = new ObjectMapper();

	@MockBean
	private UserLoginService userService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@MockBean
	private AuthenticationManager authenticationManager;

	private UserModel user;

	@BeforeAll
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		user = new UserModel();
		user.setFirstName("Ram");
		user.setLastName("Agarwal");
		user.setUsername("ram");
		user.setPassword("1234");
		user.setEmail("test@gmail.com");
	}

	@Test
	public void testRegisterUser() throws Exception {
		when(userService.saveUser(user)).thenReturn(true);
		String jsonRequest = om.writeValueAsString(user);
		mockMvc.perform(post("/api/auth/signup").content(jsonRequest).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andExpect(jsonPath("$['data']['username']").value(user.getUsername()));
	}

	@Test
	public void testRegisterExistingUsername() throws Exception {
		String jsonRequest = om.writeValueAsString(user);
		when(userService.existsByUsername(user.getUsername())).thenReturn(true);
		mockMvc.perform(post("/api/auth/signup").content(jsonRequest).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testRegisterExistingEmail() throws Exception {
		when(userService.existsByEmail(user.getEmail())).thenReturn(true);
		user.setUsername("xyz");
		String jsonRequest = om.writeValueAsString(user);
		mockMvc.perform(post("/api/auth/signup").content(jsonRequest).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testLoginUser() throws Exception {
		UserModel u = new UserModel();
		u.setUsername("ram");
		u.setPassword("1234");
		String jsonRequest = om.writeValueAsString(u);
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(u, null,
				u.getAuthorities());
		when(userService.loadUserByUsername(u.getUsername())).thenReturn(u);
		when(authenticationManager.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class)))
				.thenReturn(authentication);
		mockMvc.perform(post("/api/auth/login").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(jsonRequest)).andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void testLoginUserWithWrongPassword() throws Exception {
		UserModel u = new UserModel();
		u.setUsername("ram");
		u.setPassword("123s4");
		String jsonRequest = om.writeValueAsString(u);
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(u, null,
				u.getAuthorities());
		when(userService.loadUserByUsername(u.getUsername())).thenReturn(u);
		when(authenticationManager.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class)))
				.thenThrow(new RuntimeException("wrong password"));
		mockMvc.perform(post("/api/auth/login").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(jsonRequest)).andExpect(status().isBadRequest()).andDo(print());
	}

	@Test
	public void testLoginUserWithWrongUsername() throws Exception {
		UserModel u = new UserModel();
		u.setUsername("cs");
		u.setPassword("1234");
		String jsonRequest = om.writeValueAsString(u);
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(u, null,
				u.getAuthorities());
		when(userService.loadUserByUsername(u.getUsername())).thenThrow(new UsernameNotFoundException("not found user"));
		when(authenticationManager.authenticate(Mockito.any(UsernamePasswordAuthenticationToken.class)))
				.thenReturn(authentication);
		mockMvc.perform(post("/api/auth/login").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).content(jsonRequest)).andExpect(status().isNotFound())
				.andDo(print());
	}

}
