package com.stackroute.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.stackroute.model.UserModel;

public interface UserLoginService extends UserDetailsService {

	public boolean existsByUsername(String userName);

	public boolean existsByEmail(String email);

	public boolean saveUser(UserModel user);

}
