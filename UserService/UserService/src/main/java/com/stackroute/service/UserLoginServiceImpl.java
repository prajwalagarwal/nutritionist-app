package com.stackroute.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.stackroute.model.UserModel;
import com.stackroute.repository.UserRepository;

@Service
public class UserLoginServiceImpl implements UserLoginService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserModel user = userRepository.findById(username).orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
		return user;
	}
	
	@Override
	public boolean existsByUsername(String userName) {
		return userRepository.findById(userName).isPresent();
	}

	@Override
	public boolean existsByEmail(String emailId) {
		// TODO Auto-generated method stub
		if(userRepository.findByEmail(emailId)!=null) {
			return true;
		}
		return false;
	}

	@Override
	public boolean saveUser(UserModel user) {
		// TODO Auto-generated method stub
		if(userRepository.findById(user.getUsername()).isPresent()) {
			return false;
		}
		userRepository.save(user);
		return true;
		
	}

}