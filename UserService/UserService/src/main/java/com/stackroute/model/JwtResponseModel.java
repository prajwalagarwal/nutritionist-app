package com.stackroute.model;

import java.io.Serializable;

public class JwtResponseModel implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;
	private final String apiKey;

	public JwtResponseModel(String jwttoken,String apiKey) {
		this.jwttoken = jwttoken;
		this.apiKey=apiKey;
	}

	public String getToken() {
		return this.jwttoken;
	}
	public String getApiKey() {
		return this.apiKey;
	}
}