package com.stackroute.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.config.JwtTokenUtil;
import com.stackroute.model.JwtResponseModel;
import com.stackroute.model.UserModel;
import com.stackroute.service.ResponseHandler;
import com.stackroute.service.UserLoginService;

@RestController
@CrossOrigin
@RequestMapping("/api/auth")
public class UserServiceController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserLoginService userLoginService;
	
	@Autowired
	PasswordEncoder encoder;

	final String apiKey="ooGcC3WbMT5Z2bm8gr0uBTYGIjcdWxOIEClmmthG";
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody UserModel loginDetail) {

		try {
			
			String userId = loginDetail.getUsername();
			String password = loginDetail.getPassword();
			if (userId == null || password == null) {
				throw new Exception("Username or password can not be empty");
			}
			final UserDetails userDetails = userLoginService.loadUserByUsername(loginDetail.getUsername());
			authenticate(loginDetail.getUsername(), loginDetail.getPassword());
			final String token = jwtTokenUtil.generateToken(userDetails);
			return ResponseHandler.generateResponse("Authentication success", HttpStatus.OK,
					new JwtResponseModel(token,this.apiKey));

		} catch (UsernameNotFoundException exception) {
			return ResponseHandler.generateResponse("Username not found", HttpStatus.NOT_FOUND, null);
		} catch (Exception exception) {
			return ResponseHandler.generateResponse(exception.getMessage(), HttpStatus.BAD_REQUEST, null);
		}
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@RequestBody UserModel userModel) {
		if (userLoginService.existsByUsername(userModel.getUsername())) {
			return ResponseHandler.generateResponse("Username is already taken!", HttpStatus.BAD_REQUEST, null);
		}
		if (userLoginService.existsByEmail(userModel.getEmail())) {
			return ResponseHandler.generateResponse("Email is already in use!", HttpStatus.BAD_REQUEST, null);
		}
		try {
			// Create new user's account
			String userId = userModel.getUsername();
			String password = userModel.getPassword();
			if (userId == null || password == null) {
				throw new Exception("username or password can not be empty");
			}
			UserModel user = new UserModel(userModel.getUsername(), encoder.encode(userModel.getPassword()),
					userModel.getEmail(), userModel.getFirstName(), userModel.getLastName());

			userLoginService.saveUser(user);
			return ResponseHandler.generateResponse("User registered successfully!", HttpStatus.OK, user);
		} catch (Exception exception) {
			return ResponseHandler.generateResponse(exception.getMessage(), HttpStatus.BAD_REQUEST, null);
		}
	}

	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			System.out.println(e.getMessage());
			throw new Exception("INVALID CREDENTIALS", e);
		}
	}

}
