1. Home Page
    -Home page should have proper UI elements to search for food based on 
     food description, food type, published date.

     food description- 
        Query Field- description:skor only matches foods that have “skor” in their description
        
     published date- filled in post body
        
     food type- foodCategory:Branded

2. Search Food Module
    User should able to search with different criteria (based on availability of rest end point) like
    a) Ingredients
    b) Brand Owner
    c) description
    d) published date
    f) Food type:foodCategory

    Sorting should be implemented (based on selected field like description, food type, published date)
    Sort By-
        a) description
        b) food type 
        c) brand

3. Recommended Food Module
    Fetch all the food items based on the brand of food which the user ordered mostly.Create a map and see most wishlist items of brand.
    Check brands present in wishlist and call api for food items with having that brand
    

