import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AuthenticationService } from 'src/app/services/authentication-service/authentication.service';
import { Constants } from 'src/app/shared/constants/constants';
import { ErrorDialogService } from 'src/app/shared/error-dialog/error-dialog.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  signUpForm!: FormGroup;

  constructor(private errorService: ErrorDialogService, private dialog: MatDialog, private auth: AuthService, private router: Router, private fb: FormBuilder, private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
   
      this.initialiseForm();
  }
  initialiseForm() {
    this.signUpForm = this.fb.group({
      "email": ['', [Validators.required, Validators.email]],
      "password": ['', Validators.required],
      "firstName":['',Validators.required],
      "lastName":['',Validators.required],
      "username":['',Validators.required],
    })
  }
  signUp() {
    this.authenticationService.registerUser(this.signUpForm.value).subscribe((res: any) => {
      if(res.message='success'){
          let data: any = {};
          data.reason = Constants.USER_REGISTERED;
          data.showMessage = true;
          this.errorService.openDialog(data);
      }
    })
  }
  loginHome(){
    this.router.navigate(['login-home/login']);
  }
}
