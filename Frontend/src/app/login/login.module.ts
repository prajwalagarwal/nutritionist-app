import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppMaterialModule } from '../app-material/app-material.module';
import { ErrorDialogComponent } from '../shared/error-dialog/error-dialog.component';
import { ErrorDialogService } from '../shared/error-dialog/error-dialog.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginRoutingModule } from './login-routing.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [
    ForgotPasswordComponent,
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    AppMaterialModule,
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  entryComponents: [
    ErrorDialogComponent,
    ForgotPasswordComponent,
    ErrorDialogService
  ]
})
export class LoginModule { }
