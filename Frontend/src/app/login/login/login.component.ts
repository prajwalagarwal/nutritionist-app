import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { AuthenticationService } from 'src/app/services/authentication-service/authentication.service';
import { Constants } from 'src/app/shared/constants/constants';
import { ErrorDialogService } from 'src/app/shared/error-dialog/error-dialog.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(private errorService: ErrorDialogService, private dialog: MatDialog, private auth: AuthService, private router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      this.router.navigate(['dashboard']);
    }
    else {
      this.initialiseForm();
    }
  }
  initialiseForm() {
    this.loginForm = this.fb.group({
      "username": ['', [Validators.required]],
      "password": ['', Validators.required]
    })
  }
  login() {
    this.auth.logInUser(this.loginForm.value).subscribe((res: any) => {
      this.router.navigate(['dashboard']);
    })
  }
  forgotPassword() {
    const dialog = this.dialog.open(ForgotPasswordComponent)
  }
  signUp() {
    this.router.navigate(['login-home/register']);
  }

}
