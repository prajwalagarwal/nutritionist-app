import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss']
})
export class ConfirmDeleteComponent implements OnInit {

  dataToReturn!: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private matDialogRef: MatDialogRef<ConfirmDeleteComponent>) {
    this.data = data;
  }

  ngOnInit() {
  }
  onConfirmation() {
    this.dataToReturn = true;
    this.matDialogRef.close(this.dataToReturn);
  }

}
