export class Constants {
    public static BASE_URL: string = "http://localhost:7001/";
    public static BASE_URL_FOR_WISHLIST: string="http://localhost:7002/";

    public static LOGIN_API_END_POINT: string = "api/auth/";
    public static WISHLIST_API_END_POINT: string = "api/wish-list";
    public static PRODUCT_API_END_POINT : string= "product.review.management/v1/products/";
    public static REVIEW_API_END_POINT : string= "product.review.management/v1/review/";


    public static RESET_PASSWORD: string = "reset password";
    public static INVALID_CREDENTIALS: string = "Invalid credentials";
    public static USER_REGISTERED: string = "User registered successfully";
    public static ADD_PRODUCT: string = "add this Product";
    public static PRODUCT_ADDED: string = "Product added successfully";
    public static REVIEW_DELETED: string = "Review deleted successfully";
    public static REVIEW_APPROVED: string = "Review approved successfully";
    public static REVIEW_ADDED: string = "Review added successfully. It will be displayed once approved.";
    public static APPROVE_REVIEW: string = "approve this Review";
    public static DELETE_REVIEW: string = "delete this Review";
    public static SEARCH_CRITERIA  = [ "Description","Brand Name", "Published Date", "Ingredients","Category"];
    public static SEARCH_CRITERIA_TEXT = [ "description:","brandName:", "publishedDate:", "ingredients:","foodCategory:"];
    public static SORT_CRITERIA=['Description','Food Category','Brand'];
    public static SORT_CRITERIA_TEXT=['description.keyword','foodCategory.keyword','brandName.keyword'];
    public static FDC_API_URL: string="https://api.nal.usda.gov/fdc/v1/";
}