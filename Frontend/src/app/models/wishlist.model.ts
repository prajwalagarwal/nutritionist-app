import { NutrientModel } from "./nutrient.model";

export class WishlistModel {
    fdcId!:number;
    dataType!:string;
    description!:string;
    publishedDate!:string;
    brandName!:string;
    ingredients!:string;
    additionalDescriptions!:string;
    foodCategory!:string;
    userId!:string;
    foodNutrients!: NutrientModel[];
}