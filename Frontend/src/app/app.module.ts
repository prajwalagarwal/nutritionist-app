import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AppMaterialModule } from './app-material/app-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RequestInterceptorInterceptor } from './services/interceptor/request-interceptor.interceptor';
import { ConfirmDeleteComponent } from './shared/confirm-delete/confirm-delete.component';
import { ErrorDialogComponent } from './shared/error-dialog/error-dialog.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WishlistComponent } from './wishlist/wishlist.component';
import { ViewFoodComponent } from './view-food/view-food.component';
import { SearchFoodComponent } from './search-food/search-food.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { RecommendedFoodComponent } from './recommended-food/recommended-food.component';
import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    ErrorDialogComponent,
    ConfirmDeleteComponent,
    DashboardComponent,
    WishlistComponent,
    ViewFoodComponent,
    SearchFoodComponent,
    RecommendedFoodComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    ToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    NgHttpLoaderModule.forRoot(),
  ],
  providers: [
    AppMaterialModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptorInterceptor,
      multi: true
    }
  ],
  entryComponents: [
    ErrorDialogComponent,
    ConfirmDeleteComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
