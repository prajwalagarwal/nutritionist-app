import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RecommendedFoodComponent } from './recommended-food/recommended-food.component';
import { SearchFoodComponent } from './search-food/search-food.component';
import { WishlistComponent } from './wishlist/wishlist.component';

const routes: Routes = [
  {
    path: 'login-home', loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    data: { showHeader: false }
  },
  {
    path: 'dashboard', component: DashboardComponent
  },
  {
    path: 'search-food', component: SearchFoodComponent
  },
  {
    path: 'wishlist-items', component: WishlistComponent
  },
  {
    path: 'recommended-food', component: RecommendedFoodComponent
  },
  {
    path: '', redirectTo: 'login-home', pathMatch: 'full'
  },
  {
    path: '**', component: NotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
