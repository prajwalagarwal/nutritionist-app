import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WishlistModel } from 'src/app/models/wishlist.model';
import { Constants } from 'src/app/shared/constants/constants';
import { FoodService } from '../food-service/food.service';
import { WishlistService } from '../wishlist-service/wishlist.service';

@Injectable({
  providedIn: 'root'
})
export class RecommendationService {
  fdcApiUrlURL: string;
  baseUrl!: string
  wishListItems!: WishlistModel[]


  constructor(private wishlistService: WishlistService, private foodService: FoodService) {
    this.fdcApiUrlURL = Constants.FDC_API_URL;
    this.baseUrl = Constants.BASE_URL_FOR_WISHLIST;
  }

  getRecommendationFoodByBrand() {
    this.wishlistService.getAllWishlistItems().subscribe((res: any) => {
      this.wishListItems = res.data;
      let brandName = '';
      let maxBrand = 0;
      let noOfProductPerBrand = new Map<string, number>();
      this.wishListItems.forEach((item: WishlistModel) => {
        if (noOfProductPerBrand.has(item.brandName)) {
          let count = noOfProductPerBrand.get(item.brandName) || 0;
          noOfProductPerBrand.set(item.brandName, ++count);
          if (maxBrand <= count) {
            brandName = item.brandName;
            maxBrand = count;
          }
        } else {
          noOfProductPerBrand.set(item.brandName, 1);
          if (maxBrand <= 1) {
            brandName = item.brandName;
            maxBrand = 1;
          }
        }
      })
      console.log(brandName)
      console.log(maxBrand)
      if (maxBrand > 0) {
        let obj: any = {}
        obj.query = Constants.SEARCH_CRITERIA_TEXT[1] + brandName;
        obj.sortBy = Constants.SORT_CRITERIA_TEXT[2];
        this.foodService.searchFood(obj).subscribe((res) => {
          return res;
        })
      }
    })
  }
}
