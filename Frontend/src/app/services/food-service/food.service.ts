import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Constants } from 'src/app/shared/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class FoodService {
  fdcApiUrlURL: string;

  constructor(private http: HttpClient) {
    this.fdcApiUrlURL = Constants.FDC_API_URL;
  }

  searchFood(searchFoodObject: any) {
    const url = this.fdcApiUrlURL+"foods/search";
    return this.http.post(url, searchFoodObject)
  }

  getNutrientOfFdcId(fdcId:number){
    const url =this.fdcApiUrlURL+"food/"+fdcId;
    return this.http.get(url);
  }



}
