import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WishlistModel } from 'src/app/models/wishlist.model';
import { Constants } from 'src/app/shared/constants/constants';

@Injectable({
  providedIn: 'root'
})
export class WishlistService {

  baseUrl!: string
  constructor(private http: HttpClient) {
    this.baseUrl = Constants.BASE_URL_FOR_WISHLIST;
  }

  addToWishlist(wishlistItem:WishlistModel){
    const url=this.baseUrl+Constants.WISHLIST_API_END_POINT;
    return this.http.post(url,wishlistItem);
  }

  deleteFromWishlist(fdcId:any){
    const url=this.baseUrl+Constants.WISHLIST_API_END_POINT+"/"+fdcId;
    return this.http.delete(url);
  }

  getAllWishlistItems(){
    const url=this.baseUrl+Constants.WISHLIST_API_END_POINT;
    return this.http.get(url);
  }
}
