import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap, shareReplay } from 'rxjs';
import { Constants } from 'src/app/shared/constants/constants';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseURL!: string;

  constructor(private http: HttpClient) {
    this.baseURL = Constants.BASE_URL;
  }

  logInUser(loginInfo: any) {
    const url = this.baseURL + Constants.LOGIN_API_END_POINT+"login";
    return this.http.post(url, loginInfo)
      .pipe(
        tap((response:any) => this.setSession(response.data)),
        shareReplay()
      );
  }

  setSession(authResult: any) {
    this.logOut();
    localStorage.setItem('token', authResult.token);
    localStorage.setItem('apiKey', authResult.apiKey);
    this.deocdeJWTToken(authResult.token);
  }

  logOut() {
    localStorage.clear();
    sessionStorage.clear();
  }

  public isLoggedIn() {
    let isLoggedIn: boolean;
    var daysToExpire = Math.floor(Number(localStorage.getItem('expires_in')) / (3600 * 24));
    if (daysToExpire > 0)
      isLoggedIn = true;
    else
      isLoggedIn = false;
    return isLoggedIn;

  }
  deocdeJWTToken(token: string) {
    let decodedToken = jwt_decode(token);
    console.log(decodedToken)
    localStorage.setItem('user', decodedToken.sub);
    localStorage.setItem('expires_in', decodedToken.exp);
  }


}
