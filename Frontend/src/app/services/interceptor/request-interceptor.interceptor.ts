import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, map, Observable, throwError } from 'rxjs';
import { ErrorDialogService } from 'src/app/shared/error-dialog/error-dialog.service';

@Injectable()
export class RequestInterceptorInterceptor implements HttpInterceptor {

  constructor(private errorService: ErrorDialogService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let modified = this.addTokenToRequest(request);
    return next.handle(modified).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data: any = {};
        console.log(request)
        if (error.status === 401) {
          data = this.prepareDataObjectForUnAuthorizedError(error);
        } else if (error.status === 404) {
          data = this.prepareDataObjectForResourceNotFoundError(error);
        } else if (error.status === 400) {
          data = this.prepareDataObjectForBadRequestError(error);
          if (data.reason == "Product already present with this code") {
            data.reason = "Product already present with this code. Redirecting to the product. Please wait!!"
            data.productCode = request.body.productCode;
          }
        } else {
          data = this.prepareDataObjectForInternalServerError(error);
        }
        this.errorService.openDialog(data);
        return throwError(() => error);
      })
    );
  }
  prepareDataObjectForResourceNotFoundError(error: HttpErrorResponse) {
    let data: any = {};
    data.reason = error.error.message;
    data.showMessage = true;
    return data;
  }

  prepareDataObjectForUnAuthorizedError(error: HttpErrorResponse) {
    let data: any = {};
    data.reason = error.error.message;
    data.showMessage = true;
    return data;
  }

  prepareDataObjectForBadRequestError(error: HttpErrorResponse) {
    let data: any = {};
    data.reason = error.error.message;
    data.showMessage = true;
    console.log(error)
    return data;
  }

  prepareDataObjectForInternalServerError(error: HttpErrorResponse) {
    let data: any = {};
    data.reason = error.error.message;
    data.showMessage = true;
    return data;
  }
  addTokenToRequest(req: HttpRequest<any>) {
    let modified;
    if (req.url.indexOf('auth') != -1) {
      //Change to BASIC_TOKEN_NEW_PROD to generate an artifact for PROD
      modified = req.clone();
    } else if (req.url.indexOf('api.nal.usda.gov') != -1) {
      modified = req.clone({ setParams: { "api_key": localStorage.getItem('apiKey') || "" } })
    }
    else {
      modified = req.clone({ setHeaders: { 'Authorization': 'Bearer' + ' ' + localStorage.getItem('token') } });
    }
    return modified;
  }

}
