import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommendedFoodComponent } from './recommended-food.component';

describe('RecommendedFoodComponent', () => {
  let component: RecommendedFoodComponent;
  let fixture: ComponentFixture<RecommendedFoodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecommendedFoodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendedFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
