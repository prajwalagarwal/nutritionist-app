import { Component, OnInit } from '@angular/core';
import { WishlistModel } from '../models/wishlist.model';
import { FoodService } from '../services/food-service/food.service';
import { RecommendationService } from '../services/recommendation-service/recommendation.service';
import { WishlistService } from '../services/wishlist-service/wishlist.service';
import { Constants } from '../shared/constants/constants';

@Component({
  selector: 'app-recommended-food',
  templateUrl: './recommended-food.component.html',
  styleUrls: ['./recommended-food.component.scss']
})
export class RecommendedFoodComponent implements OnInit {

  foodSearchResult: any;
  wishListItems: any;

  constructor(private wishlistService: WishlistService, private foodService: FoodService) { }

  ngOnInit(): void {
    this.getRecommendationFoodByBrand();
  }
  
  getRecommendationFoodByBrand() {
    this.wishlistService.getAllWishlistItems().subscribe((res: any) => {
      this.wishListItems = res.data;
      let brandName = '';
      let maxBrand = 0;
      let noOfProductPerBrand = new Map<string, number>();
      this.wishListItems.forEach((item: WishlistModel) => {
        if (noOfProductPerBrand.has(item.brandName)) {
          let count = noOfProductPerBrand.get(item.brandName) || 0;
          noOfProductPerBrand.set(item.brandName, ++count);
          if (maxBrand <= count) {
            brandName = item.brandName;
            maxBrand = count;
          }
        } else {
          noOfProductPerBrand.set(item.brandName, 1);
          if (maxBrand <= 1) {
            brandName = item.brandName;
            maxBrand = 1;
          }
        }
      })
      console.log(brandName)
      console.log(maxBrand)
      if (maxBrand > 0) {
        let obj: any = {}
        obj.query = Constants.SEARCH_CRITERIA_TEXT[1] + brandName;
        obj.sortBy = Constants.SORT_CRITERIA_TEXT[2];
        this.foodService.searchFood(obj).subscribe((res) => {
          this.foodSearchResult = res;
          console.log(this.foodSearchResult)
        })
      }
    })
  }

}
