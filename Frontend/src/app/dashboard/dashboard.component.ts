import { Component, OnInit } from '@angular/core';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { WishlistModel } from '../models/wishlist.model';
import { FoodService } from '../services/food-service/food.service';
import { WishlistService } from '../services/wishlist-service/wishlist.service';
import { Constants } from '../shared/constants/constants';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  keyWord!: string
  valueToSearch!: string;
  searchCriteria = Constants.SEARCH_CRITERIA;
  searchCriteriaText = Constants.SEARCH_CRITERIA_TEXT;
  selectedSearchCriteria!: string
  dateToSearch!: Date;
  recommendedFood: any;
  wishListItems!: WishlistModel[];

  constructor(private router: Router,private foodService:FoodService,private wishlistService:WishlistService) { }

  ngOnInit(): void {
    this.selectedSearchCriteria = this.searchCriteria[0];
    this.getRecommendationFoodByBrand();
  }

  search() {
    if (this.selectedSearchCriteria !== this.searchCriteria[2])
      this.router.navigate(['search-food'], { queryParams: { "query": this.valueToSearch, "search-by": this.selectedSearchCriteria } });
    else{
      const yyyy = this.dateToSearch.getFullYear();
      let mm: any = this.dateToSearch.getMonth() + 1; // Months start at 0!
      let dd: any = this.dateToSearch.getDate();

      if (dd < 10) dd = '0' + dd;
      if (mm < 10) mm = '0' + mm;
      let today: string
      today = yyyy + '-' + mm + '-' + dd;
      this.router.navigate(['search-food'], { queryParams: { "query": this.dateToSearch, "search-by": this.selectedSearchCriteria } });
    }

  }
  
  getRecommendationFoodByBrand() {
    this.wishlistService.getAllWishlistItems().subscribe((res: any) => {
      this.wishListItems = res.data;
      let brandName = '';
      let maxBrand = 0;
      let noOfProductPerBrand = new Map<string, number>();
      this.wishListItems.forEach((item: WishlistModel) => {
        if (noOfProductPerBrand.has(item.brandName)) {
          let count = noOfProductPerBrand.get(item.brandName) || 0;
          noOfProductPerBrand.set(item.brandName, ++count);
          if (maxBrand <= count) {
            brandName = item.brandName;
            maxBrand = count;
          }
        } else {
          noOfProductPerBrand.set(item.brandName, 1);
          if (maxBrand <= 1) {
            brandName = item.brandName;
            maxBrand = 1;
          }
        }
      })
      console.log(brandName)
      console.log(maxBrand)
      if (maxBrand > 0) {
        let obj: any = {}
        obj.query = Constants.SEARCH_CRITERIA_TEXT[1] + brandName;
        obj.sortBy = Constants.SORT_CRITERIA_TEXT[2];
        this.foodService.searchFood(obj).subscribe((res) => {
          this.recommendedFood = res;
          if(this.recommendedFood.foods.length>8){
            this.recommendedFood.foods.splice(8,this.recommendedFood.foods.length);
          }
          console.log(this.recommendedFood);
        })
      }
    })
  }
}
