import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { WishlistModel } from '../models/wishlist.model';
import { RecommendationService } from '../services/recommendation-service/recommendation.service';
import { WishlistService } from '../services/wishlist-service/wishlist.service';
import { ConfirmDeleteComponent } from '../shared/confirm-delete/confirm-delete.component';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {

  wishListItems!: WishlistModel[]
  nutrientList: any = [[]]
  panelOpenState: boolean = false;
  constructor(private wishlistService: WishlistService,private dialog:MatDialog,private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.getWishlistItems();
  }

  getWishlistItems() {
    this.wishlistService.getAllWishlistItems().subscribe((res: any) => {
      this.wishListItems = res.data;
      console.log(this.wishListItems)
    })
  }

  removeFromFavourite(fdcId: number) {
    const dialog=this.dialog.open(ConfirmDeleteComponent,{data:{
      text:"remove from wishlist"
    }})
    dialog.afterClosed().subscribe((res:any)=>{
      if(res){
        this.wishlistService.deleteFromWishlist(fdcId).subscribe((res: any) => {
          this.toastrService.success("Food removed from wishlist", "Success");
          this.getWishlistItems();
        })
      }
    })
  }

  closePanel() {
    this.panelOpenState = false;
  }

  openPanel() {
    this.panelOpenState = true;
  }
}
