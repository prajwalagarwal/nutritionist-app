import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivationStart, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'nutritionistApp';
  userName!: string;
  showHeader: boolean = false;
  constructor(private router: Router) { }

  ngOnInit(): void {

    this.router.events.subscribe(event => {
      if (event instanceof ActivationStart) {
        console.log(event);
        this.userName = localStorage.getItem('user') || '';
        console.log(this.userName);
        this.showHeader = event.snapshot.data['showHeader'] !== false;
      }
    });
  }


  logOut() {
    localStorage.clear();
    this.router.navigate(['login-home']);
  }
}
