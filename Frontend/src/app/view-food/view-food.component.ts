import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { WishlistModel } from '../models/wishlist.model';
import { WishlistService } from '../services/wishlist-service/wishlist.service';
import { ConfirmDeleteComponent } from '../shared/confirm-delete/confirm-delete.component';

@Component({
  selector: 'app-view-food',
  templateUrl: './view-food.component.html',
  styleUrls: ['./view-food.component.scss']
})
export class ViewFoodComponent implements OnInit {

  @Input() foodList: any
  foodArray: any
  panelOpenState: boolean = false;
  wishListItems!: WishlistModel[]
  wishListItemIdArray!: any[]
  constructor(private wishlistService: WishlistService, private dialog: MatDialog, private toastrService: ToastrService) { }

  ngOnInit(): void {
    console.log(this.foodList)
    this.foodArray = this.foodList.foods;
    this.getWishlistItems();
  }
  getWishlistItems() {
    this.wishlistService.getAllWishlistItems().subscribe((res: any) => {
      this.wishListItems = res.data;
      this.wishListItemIdArray = []
      this.updateWishListItemIdArrayList();
      console.log(this.wishListItems)
    })
  }

  updateWishListItemIdArrayList() {
    this.wishListItems.forEach(item => {
      this.wishListItemIdArray.push(item.fdcId);
    })
  }

  removeFromFavourite(fdcId: number) {
    const dialog = this.dialog.open(ConfirmDeleteComponent, {
      data: {
        text: "remove from wishlist"
      }
    })
    dialog.afterClosed().subscribe((res: any) => {
      if (res) {
        this.wishlistService.deleteFromWishlist(fdcId).subscribe((res: any) => {
          this.getWishlistItems();
          this.toastrService.success("Food removed from wishlist", "Success");
        })
      }
    })
  }

  addToFavourite(foodItem: any) {
    let obj: WishlistModel = {
      additionalDescriptions: foodItem.additionalDescriptions,
      fdcId: foodItem.fdcId,
      dataType: foodItem.dataType,
      description: foodItem.description,
      publishedDate: foodItem.publishedDate,
      brandName: foodItem.brandName,
      ingredients: foodItem.ingredients,
      userId: foodItem.userId,
      foodCategory: foodItem.foodCategory,
      foodNutrients: foodItem.foodNutrients
    }
    this.wishlistService.addToWishlist(obj).subscribe((res: any) => {
      this.toastrService.success("Food added to wishlist", "Success");
      this.getWishlistItems();
      console.log(res);
    })
  }

  closePanel() {
    this.panelOpenState = false;
  }

  openPanel() {
    this.panelOpenState = true;
  }

}
