import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { FoodService } from '../services/food-service/food.service';
import { Constants } from '../shared/constants/constants';

@Component({
  selector: 'app-search-food',
  templateUrl: './search-food.component.html',
  styleUrls: ['./search-food.component.scss']
})
export class SearchFoodComponent implements OnInit {
  selectedSortCriteria!: string
  valueToSearch!: any;
  searchCriteria = Constants.SEARCH_CRITERIA;
  searchCriteriaText = Constants.SEARCH_CRITERIA_TEXT;
  sortCriteria = Constants.SORT_CRITERIA;
  sortCriteriaText = Constants.SORT_CRITERIA_TEXT;
  selectedSearchCriteria!: string
  foodSearchResult: any;
  dateToSearch!: Date;
  pageIndex:number=0;
  pageEvent: any
  constructor(private router: Router, private route: ActivatedRoute, private foodService: FoodService) { }

  ngOnInit(): void {
    this.selectedSearchCriteria = this.route.snapshot.queryParams['search-by'] || '';
    this.selectedSortCriteria = this.sortCriteria[0];
    if (!this.selectedSearchCriteria) {
      this.selectedSearchCriteria = this.searchCriteria[0];
    }
    if (this.selectedSearchCriteria === this.searchCriteria[2]) {
      this.dateToSearch = new Date(this.route.snapshot.queryParams['query'] || '');
    } else {
      this.valueToSearch = this.route.snapshot.queryParams['query'] || '';
    }
    console.log(this.dateToSearch)
    if (this.valueToSearch || this.dateToSearch)
      this.search();
  }
  public handlePage(e: any) {
    this.pageIndex = e.pageIndex;
    this.search()
  }

  search() {
    this.router.navigate(['search-food'], {
      queryParams: {
        'query': null,
        'search-by': null,
      },
      queryParamsHandling: 'merge'
    })
    let obj: any = {}
    this.foodSearchResult = null
    switch (this.selectedSearchCriteria) {
      case this.searchCriteria[0]:
        obj.query = this.searchCriteriaText[0] + this.valueToSearch;
        break;
      case this.searchCriteria[1]:
        obj.query = this.searchCriteriaText[1] + this.valueToSearch;
        break;
      case this.searchCriteria[2]:
        const yyyy = this.dateToSearch.getFullYear();
        let mm: any = this.dateToSearch.getMonth() + 1; // Months start at 0!
        let dd: any = this.dateToSearch.getDate();

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        let today: string
        today = yyyy + '-' + mm + '-' + dd;
        obj.query = this.searchCriteriaText[2] + today;
        console.log(this.valueToSearch)
        console.log(new Date(this.valueToSearch))
        break;
      case this.searchCriteria[3]:
        obj.query = this.searchCriteriaText[3] + this.valueToSearch;
        break;
      case this.searchCriteria[4]:
        obj.query = this.searchCriteriaText[4] + this.valueToSearch;
        break;
    }
    switch (this.selectedSortCriteria) {
      case this.sortCriteria[0]:
        obj.sortBy = this.sortCriteriaText[0];
        break;
      case this.sortCriteria[1]:
        obj.sortBy = this.sortCriteriaText[1];
        break;
      case this.sortCriteria[2]:
        obj.sortBy = this.sortCriteriaText[2];
        break;
    }
    obj.pageNumber=this.pageIndex+1;
    this.foodService.searchFood(obj).subscribe((res: any) => {
      this.foodSearchResult = res;
    })
  }
  triggerSearch() {
    if (this.valueToSearch || this.dateToSearch) {
      this.search();
    }
  }
  // nextPage() {
  //   if (this.foodSearchResult.currentpage < this.foodSearchResult.totalPages) {
  //     let obj: any = {}
  //     switch (this.selectedSearchCriteria) {
  //       case this.searchCriteria[0]:
  //         obj.query = this.searchCriteriaText[0] + this.valueToSearch;
  //         break;
  //       case this.searchCriteria[1]:
  //         obj.query = this.searchCriteriaText[1] + this.valueToSearch;
  //         break;
  //       case this.searchCriteria[2]:
  //         const yyyy = this.dateToSearch.getFullYear();
  //         let mm: any = this.dateToSearch.getMonth() + 1; // Months start at 0!
  //         let dd: any = this.dateToSearch.getDate();

  //         if (dd < 10) dd = '0' + dd;
  //         if (mm < 10) mm = '0' + mm;
  //         let today: string
  //         today = yyyy + '-' + mm + '-' + dd;
  //         obj.query = this.searchCriteriaText[2] + today;
  //         console.log(this.valueToSearch)
  //         console.log(new Date(this.valueToSearch))
  //         break;
  //       case this.searchCriteria[3]:
  //         obj.query = this.searchCriteriaText[3] + this.valueToSearch;
  //         break;
  //       case this.searchCriteria[4]:
  //         obj.query = this.searchCriteriaText[4] + this.valueToSearch;
  //         break;
  //     }
  //     switch (this.selectedSortCriteria) {
  //       case this.sortCriteria[0]:
  //         obj.sortBy = this.sortCriteriaText[0];
  //         break;
  //       case this.sortCriteria[1]:
  //         obj.sortBy = this.sortCriteriaText[1];
  //         break;
  //       case this.sortCriteria[2]:
  //         obj.sortBy = this.sortCriteriaText[2];
  //         break;
  //       case this.sortCriteria[3]:
  //         obj.sortBy = this.sortCriteriaText[3];
  //         break;
  //     }
  //     obj.pageNumber = this.foodSearchResult.currentpage + 1;
  //     this.foodService.searchFood(obj).subscribe((res: any) => {
  //       this.foodSearchResult = res;
  //     })
  //   }
  // }
  // previousPage() {
  //   if (this.foodSearchResult.currentpage != this.foodSearchResult.totalPages && this.foodSearchResult.currentpage > 1) {
  //     let obj: any = {}
  //     switch (this.selectedSearchCriteria) {
  //       case this.searchCriteria[0]:
  //         obj.query = this.searchCriteriaText[0] + this.valueToSearch;
  //         break;
  //       case this.searchCriteria[1]:
  //         obj.query = this.searchCriteriaText[1] + this.valueToSearch;
  //         break;
  //       case this.searchCriteria[2]:
  //         const yyyy = this.dateToSearch.getFullYear();
  //         let mm: any = this.dateToSearch.getMonth() + 1; // Months start at 0!
  //         let dd: any = this.dateToSearch.getDate();

  //         if (dd < 10) dd = '0' + dd;
  //         if (mm < 10) mm = '0' + mm;
  //         let today: string
  //         today = yyyy + '-' + mm + '-' + dd;
  //         obj.query = this.searchCriteriaText[2] + today;
  //         console.log(this.valueToSearch)
  //         console.log(new Date(this.valueToSearch))
  //         break;
  //       case this.searchCriteria[3]:
  //         obj.query = this.searchCriteriaText[3] + this.valueToSearch;
  //         break;
  //       case this.searchCriteria[4]:
  //         obj.query = this.searchCriteriaText[4] + this.valueToSearch;
  //         break;
  //     }
  //     switch (this.selectedSortCriteria) {
  //       case this.sortCriteria[0]:
  //         obj.sortBy = this.sortCriteriaText[0];
  //         break;
  //       case this.sortCriteria[1]:
  //         obj.sortBy = this.sortCriteriaText[1];
  //         break;
  //       case this.sortCriteria[2]:
  //         obj.sortBy = this.sortCriteriaText[2];
  //         break;
  //       case this.sortCriteria[3]:
  //         obj.sortBy = this.sortCriteriaText[3];
  //         break;
  //     }
  //     obj.pageNumber = this.foodSearchResult.currentpage + 1;
  //     this.foodService.searchFood(obj).subscribe((res: any) => {
  //       this.foodSearchResult = res;
  //     })
  //   }
  // }
}
