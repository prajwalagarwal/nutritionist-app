package com.stackroute.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stackroute.config.JwtTokenUtil;
import com.stackroute.exception.ItemAlreadyExistsException;
import com.stackroute.exception.ItemNotFoundException;
import com.stackroute.model.FoodWishListModel;
import com.stackroute.service.FoodWishListService;
import com.stackroute.service.ResponseHandler;

import io.jsonwebtoken.ExpiredJwtException;

@RestController
@CrossOrigin(origins = "http://localhost:4200",allowCredentials = "true")
@RequestMapping("/api/wish-list")
public class FoodWishListController {

	@Autowired
	private FoodWishListService foodWishListService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@PostMapping
	public ResponseEntity<?> saveNewItem(@RequestBody final FoodWishListModel item, HttpServletRequest request) {
		final String authHeader = request.getHeader("authorization");
		System.out.println(authHeader);
		final String token = authHeader.substring(7);
		String userName = null;
		try {
			userName = jwtTokenUtil.getUsernameFromToken(token);
		} catch (IllegalArgumentException e) {
			System.out.println("Unable to get JWT Token");
		} catch (ExpiredJwtException e) {
			System.out.println("JWT Token has expired");
		}
		try {
			item.setUserId(userName);
			foodWishListService.saveItem(item);
			return ResponseHandler.generateResponse("Item Added To Wishlist",HttpStatus.CREATED,item);
		} catch (ItemAlreadyExistsException e) {
			return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.CONFLICT,null);
		}
	}

	@DeleteMapping(path = "/{fdcId}")
	public ResponseEntity<?> deleteItem(@PathVariable("fdcId") final int fdcId) {
		try {
			foodWishListService.deleteItem(fdcId);
			return ResponseHandler.generateResponse("Item Deleted Sucessfully",HttpStatus.OK,null);
		} catch (ItemNotFoundException e) {
			return ResponseHandler.generateResponse(e.getMessage(),HttpStatus.CONFLICT,null);
		}
	}

	@GetMapping
	public ResponseEntity<?> getItemList(HttpServletRequest request) {
		System.out.println("enetered get wishlist items");
		final String authHeader = request.getHeader("authorization");
		System.out.println(authHeader);
		final String token = authHeader.substring(7);
		String userName = null;
		try {
			userName = jwtTokenUtil.getUsernameFromToken(token);
		} catch (IllegalArgumentException e) {
			System.out.println("Unable to get JWT Token");
		} catch (ExpiredJwtException e) {
			System.out.println("JWT Token has expired");
		}
		List<FoodWishListModel> foodWishListModel=foodWishListService.getItemList(userName);
		return ResponseHandler.generateResponse("",HttpStatus.OK,foodWishListModel);
	}

}
