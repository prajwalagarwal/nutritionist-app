package com.stackroute.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stackroute.exception.ItemAlreadyExistsException;
import com.stackroute.exception.ItemNotFoundException;
import com.stackroute.model.FoodWishListModel;
import com.stackroute.repository.FoodWishListRepository;

@Service
public class FoodWishListServiceImpl implements FoodWishListService {
	@Autowired
	private FoodWishListRepository foodWishListRepository;

	@Override
	public boolean saveItem(FoodWishListModel item) throws ItemAlreadyExistsException {
		// TODO Auto-generated method stub
		final FoodWishListModel foodItem = foodWishListRepository.findById(item.getFdcId()).orElse(null);
		if (foodItem!=null) {
			throw new ItemAlreadyExistsException("Item Already Exits in List");
		}
		foodWishListRepository.save(item);
		return true;
	}

	@Override
	public boolean deleteItem(int fdcId) throws ItemNotFoundException {
		// TODO Auto-generated method stub
		final FoodWishListModel foodItem = foodWishListRepository.findById(fdcId).orElse(null);
		if (foodItem == null) {
			throw new ItemNotFoundException("Deletion failed,Item not found");
		}
		System.out.println(foodItem.toString());
		foodWishListRepository.delete(foodItem);
		return true;
	}

	@Override
	public List<FoodWishListModel> getItemList(String username) {
		// TODO Auto-generated method stub
		List<FoodWishListModel> foodWishList = foodWishListRepository.getWishListItemsForParticularUser(username);
		return foodWishList;
	}

}
