package com.stackroute.service;

import java.util.List;

import com.stackroute.exception.ItemAlreadyExistsException;
import com.stackroute.exception.ItemNotFoundException;
import com.stackroute.model.FoodWishListModel;

public interface FoodWishListService {

	public List<FoodWishListModel> getItemList(String username);

	public boolean saveItem(FoodWishListModel item) throws ItemAlreadyExistsException;

	public boolean deleteItem(int fdcId) throws ItemNotFoundException;

}
