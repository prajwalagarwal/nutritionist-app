package com.stackroute.model;

public class FoodNutrientModel {
	private int value;
	private String unitName;
	private String nutrientName;

	public FoodNutrientModel(int value, String unitName, String nutrientName) {
		super();
		this.value = value;
		this.unitName = unitName;
		this.nutrientName = nutrientName;
	}

	public FoodNutrientModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getNutrientName() {
		return nutrientName;
	}

	public void setNutrientName(String nutrientName) {
		this.nutrientName = nutrientName;
	}

	@Override
	public String toString() {
		return "FoodNutrientModel [value=" + value + ", unitName=" + unitName + ", nutrientName=" + nutrientName + "]";
	}

}
