package com.stackroute.model;


import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("FoodWishList")
public class FoodWishListModel {
	@Id
	private int fdcId;
	private String userId;
	private String brandName;
	private String dataType;
	private String description;
	private String publishedDate;
	private String ingredients;
	private String additionalDescriptions;
	private List<FoodNutrientModel> foodNutrients;

	public FoodWishListModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FoodWishListModel(int fdcId, String userId, String brandName, String dataType, String description,
			String publishedDate, String ingredients, String additionalDescriptions,
			List<FoodNutrientModel> foodNutrients) {
		super();
		this.fdcId = fdcId;
		this.userId = userId;
		this.brandName = brandName;
		this.dataType = dataType;
		this.description = description;
		this.publishedDate = publishedDate;
		this.ingredients = ingredients;
		this.additionalDescriptions = additionalDescriptions;
		this.foodNutrients = foodNutrients;
	}

	public int getFdcId() {
		return fdcId;
	}

	public void setFdcId(int fdcId) {
		this.fdcId = fdcId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public String getAdditionalDescriptions() {
		return additionalDescriptions;
	}

	public void setAdditionalDescriptions(String additionalDescriptions) {
		this.additionalDescriptions = additionalDescriptions;
	}

	public List<FoodNutrientModel> getFoodNutrients() {
		return foodNutrients;
	}

	public void setFoodNutrients(List<FoodNutrientModel> foodNutrients) {
		this.foodNutrients = foodNutrients;
	}

	@Override
	public String toString() {
		return "FoodWishListModel [fdcId=" + fdcId + ", userId=" + userId + ", brandName=" + brandName + ", dataType="
				+ dataType + ", description=" + description + ", publishedDate=" + publishedDate + ", ingredients="
				+ ingredients + ", additionalDescriptions=" + additionalDescriptions + ", foodNutrients="
				+ foodNutrients + "]";
	}

}
