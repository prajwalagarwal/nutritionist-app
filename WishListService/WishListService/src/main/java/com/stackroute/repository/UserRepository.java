package com.stackroute.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.stackroute.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, String> {

	@Query("select u from UserModel u where u.email=?1")
	UserModel findByEmail(String emailId);

}
