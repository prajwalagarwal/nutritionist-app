package com.stackroute.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.stackroute.model.FoodWishListModel;

public interface FoodWishListRepository extends MongoRepository<FoodWishListModel, Integer> {

	@Query("{userId :?0}")  
	public List<FoodWishListModel> getWishListItemsForParticularUser(String username);
}
