package com.stackroute.exception;

@SuppressWarnings("serial")
public class ItemAlreadyExistsException extends Exception{
	
	public ItemAlreadyExistsException(String message) {
		super(message);
	}

}
