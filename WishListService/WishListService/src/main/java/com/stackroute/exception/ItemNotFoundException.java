package com.stackroute.exception;


@SuppressWarnings("serial")
public class ItemNotFoundException extends Exception{
	
	public ItemNotFoundException(String message) {
		super(message);
	}

}
