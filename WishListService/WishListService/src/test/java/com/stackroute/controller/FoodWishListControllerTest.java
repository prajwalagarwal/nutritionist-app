package com.stackroute.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stackroute.config.JwtTokenUtil;
import com.stackroute.exception.ItemAlreadyExistsException;
import com.stackroute.exception.ItemNotFoundException;
import com.stackroute.model.FoodNutrientModel;
import com.stackroute.model.FoodWishListModel;
import com.stackroute.model.UserModel;
import com.stackroute.service.FoodWishListService;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class FoodWishListControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	ObjectMapper om = new ObjectMapper();

	@MockBean
	private FoodWishListService foodWishListService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	private UserModel user;
	Optional<FoodWishListModel> options;

	private FoodWishListModel wishListModel;

	@BeforeAll
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
		wishListModel = new FoodWishListModel(1, "ram", "nestle", "Branded", "paneer", "2020-04-17", "milk", "paneer",
				new ArrayList<FoodNutrientModel>());
	}

	@Test
	public void testSaveWishList() throws Exception {
		String jsonRequest = om.writeValueAsString(wishListModel);
		when(foodWishListService.saveItem(wishListModel)).thenReturn(true);
		mockMvc.perform(post("/api/wish-list").content(jsonRequest).header("Authorization",
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYW0iLCJleHAiOjE2NTY2NTMyMjAsImlhdCI6MTY1NTU3MzIyMH0.EWBThHs1CGYvJP-cogeMmZtebObhxSvoPC9BkLtUib2nILIZd_-81xZbUcFWiuzKvxWiXsy2ZMBg_4W0fRMO9A")
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isCreated()).andDo(print());

		when(foodWishListService.saveItem(wishListModel)).thenThrow(new ItemAlreadyExistsException("exist"));
		mockMvc.perform(post("/api/wish-list").content(jsonRequest).header("Authorization",
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYW0iLCJleHAiOjE2NTY2NTMyMjAsImlhdCI6MTY1NTU3MzIyMH0.EWBThHs1CGYvJP-cogeMmZtebObhxSvoPC9BkLtUib2nILIZd_-81xZbUcFWiuzKvxWiXsy2ZMBg_4W0fRMO9A")
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isCreated()).andDo(print());
		verify(foodWishListService, times(2)).saveItem(Mockito.any(FoodWishListModel.class));
		verifyNoMoreInteractions(foodWishListService);
	}

	@Test
	public void deleteWishList() throws Exception {
		String jsonRequest = om.writeValueAsString(wishListModel);
		when(foodWishListService.saveItem(wishListModel)).thenReturn(true);

		mockMvc.perform(post("/api/wish-list").content(jsonRequest).header("Authorization",
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYW0iLCJleHAiOjE2NTY2NTMyMjAsImlhdCI6MTY1NTU3MzIyMH0.EWBThHs1CGYvJP-cogeMmZtebObhxSvoPC9BkLtUib2nILIZd_-81xZbUcFWiuzKvxWiXsy2ZMBg_4W0fRMO9A")
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isCreated()).andDo(print());
		verify(foodWishListService, times(1)).saveItem(Mockito.any(FoodWishListModel.class));
		verifyNoMoreInteractions(foodWishListService);

		when(foodWishListService.deleteItem(1)).thenReturn(true);
		mockMvc.perform(delete("/api/wish-list/" + 1).header("Authorization",
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYW0iLCJleHAiOjE2NTY2NTMyMjAsImlhdCI6MTY1NTU3MzIyMH0.EWBThHs1CGYvJP-cogeMmZtebObhxSvoPC9BkLtUib2nILIZd_-81xZbUcFWiuzKvxWiXsy2ZMBg_4W0fRMO9A")
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk()).andDo(print());

	}

	@Test
	public void deleteWishListItemNotFoundException() throws Exception {
		when(foodWishListService.deleteItem(1)).thenThrow(new ItemNotFoundException("not found"));
		mockMvc.perform(delete("/api/wish-list/" + 1).header("Authorization",
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYW0iLCJleHAiOjE2NTY2NTMyMjAsImlhdCI6MTY1NTU3MzIyMH0.EWBThHs1CGYvJP-cogeMmZtebObhxSvoPC9BkLtUib2nILIZd_-81xZbUcFWiuzKvxWiXsy2ZMBg_4W0fRMO9A")
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isConflict()).andDo(print());
	}

	@Test
	public void getWishListItems() throws Exception {
		when(foodWishListService.getItemList("ram")).thenReturn(Arrays.asList(wishListModel));
		mockMvc.perform(get("/api/wish-list").header("Authorization",
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYW0iLCJleHAiOjE2NTY2NTMyMjAsImlhdCI6MTY1NTU3MzIyMH0.EWBThHs1CGYvJP-cogeMmZtebObhxSvoPC9BkLtUib2nILIZd_-81xZbUcFWiuzKvxWiXsy2ZMBg_4W0fRMO9A")
				.contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk()).andDo(print());
	}

}
