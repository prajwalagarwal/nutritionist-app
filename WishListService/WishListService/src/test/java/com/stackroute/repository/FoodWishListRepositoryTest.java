package com.stackroute.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.stackroute.model.FoodNutrientModel;
import com.stackroute.model.FoodWishListModel;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class FoodWishListRepositoryTest {

	@Mock
	private FoodWishListRepository foodWishListRepository;

	transient Optional<FoodWishListModel> options;

	private transient FoodWishListModel foodWishListModel;

	@BeforeAll
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		foodWishListModel = new FoodWishListModel(1, "ram", "necdssstle", "Branded", "paneer", "2020-04-17", "mcdsilk",
				"paneer", new ArrayList<FoodNutrientModel>());
		options = Optional.of(foodWishListModel);
	}

	@Test
	@Order(1)
	public void testSaveItem() throws Exception {
		when(foodWishListRepository.save(foodWishListModel)).thenReturn(foodWishListModel);
		when(foodWishListRepository.findById(foodWishListModel.getFdcId())).thenReturn(options);
		foodWishListRepository.save(foodWishListModel);
		Optional<FoodWishListModel> item = foodWishListRepository.findById(foodWishListModel.getFdcId());
		assertEquals("Item saved", item.get(), foodWishListModel);
	}

	@Test
	@Order(2)
	public void getListOfWishList() throws Exception {
		List<FoodWishListModel> asList = new ArrayList<>();
		asList.add(foodWishListModel);
		when(foodWishListRepository.getWishListItemsForParticularUser("ram")).thenReturn(asList);
		List<FoodWishListModel> list = foodWishListRepository.getWishListItemsForParticularUser("ram");
		assertEquals("Correct list obtainedr", list, asList);
	}

	@Test
	@Order(3)
	public void delete() {
		doNothing().when(foodWishListRepository).delete(foodWishListModel);
		foodWishListRepository.delete(foodWishListModel);
	}

}
