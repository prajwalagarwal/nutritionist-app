package com.stackroute.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.stackroute.exception.ItemAlreadyExistsException;
import com.stackroute.exception.ItemNotFoundException;
import com.stackroute.model.FoodNutrientModel;
import com.stackroute.model.FoodWishListModel;
import com.stackroute.repository.FoodWishListRepository;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class FoodWishListServiceTest {
	@Mock
	private transient FoodWishListRepository foodWishListRepository;

	private transient FoodWishListModel foodWishListModel;

	@Mock
	private transient FoodWishListService foodWishListService;

	@InjectMocks
	private transient FoodWishListServiceImpl foodWishListServiceImpl;
	
	transient Optional<FoodWishListModel> options;

	@BeforeAll
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		foodWishListModel = new FoodWishListModel(1, "ram", "necdssstle", "Branded", "paneer", "2020-04-17", "mcdsilk",
				"paneer", new ArrayList<FoodNutrientModel>());
		options = Optional.of(foodWishListModel);
	}

	@Test()
	@Order(1)
	public void testSaveWishList() throws Exception {
		when(foodWishListRepository.save(foodWishListModel)).thenReturn(foodWishListModel);
		boolean flag; 
		try {
			flag= foodWishListServiceImpl.saveItem(foodWishListModel);
		}catch(ItemAlreadyExistsException e) {
			System.out.println(e.getMessage());
			flag=false;
		}
		assertEquals("item saved", true, flag);
	}
	
	@Test()
	@Order(2) 
	public void testSaveWishListForExistingItem() throws ItemAlreadyExistsException {
		when(foodWishListRepository.findById(foodWishListModel.getFdcId())).thenReturn(options);
		boolean flag;
		try {
			flag = foodWishListServiceImpl.saveItem(foodWishListModel);
		} catch (ItemAlreadyExistsException e) {
			flag = false;
		}
		assertEquals("item already exists", false, flag);
	}
	
	@Test()
	@Order(3)
	public void deleteSaveWishList() throws Exception {
		
		when(foodWishListRepository.findById(foodWishListModel.getFdcId())).thenReturn(options);
		doNothing().when(foodWishListRepository).delete(foodWishListModel);
		boolean flag= foodWishListServiceImpl.deleteItem(foodWishListModel.getFdcId());
		assertEquals("item deleted", true, flag);
	}
	
	@Test()
	@Order(4)
	public void deleteSaveWishListForNoItemFound() throws ItemNotFoundException {
		when(foodWishListRepository.findById(foodWishListModel.getFdcId())).thenReturn(options);
		doNothing().when(foodWishListRepository).delete(foodWishListModel);
		boolean flag; 
		try {
			flag= foodWishListServiceImpl.deleteItem(foodWishListModel.getFdcId());
		}catch(ItemNotFoundException e) {
			System.out.println(e.getMessage());
			flag=false;
		}
		assertEquals("item not found", true, flag);
	}
}
